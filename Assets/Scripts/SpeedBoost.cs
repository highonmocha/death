﻿using UnityEngine;
using System.Collections;

public class SpeedBoost : MonoBehaviour {

	public float BOOST_SPEED = -6.0f;
	public Vector2 direction = new Vector2(-1.0f, 0.0f);
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnCollisionStay2D(Collision2D collider){
		if (collider.gameObject.name == "player") {
			Vector2 new_vel = direction*BOOST_SPEED;
			collider.gameObject.rigidbody2D.velocity = new_vel;
		}
	}
}
