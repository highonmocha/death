﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	public float MOVE_SPEED = 3.0f;

	private bool is_jumping = false;

	protected Animator animator;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!animator.GetBool("tombJump")) {
			if (Input.GetKey(KeyCode.D))
			{
				Vector2 new_vel = rigidbody2D.velocity;
				new_vel.x = MOVE_SPEED;
				rigidbody2D.velocity = new_vel;
			}
			if (Input.GetKey (KeyCode.A)) 
			{
				Vector2 new_vel = rigidbody2D.velocity;
				new_vel.x = -MOVE_SPEED;
				rigidbody2D.velocity = new_vel;	
			}
			if ((Input.GetKeyUp (KeyCode.A) || Input.GetKeyUp (KeyCode.D)) && !is_jumping) {
				Vector2 new_vel = rigidbody2D.velocity;
				new_vel.x = 0.0f;
				rigidbody2D.velocity = new_vel;
			}
			// fix so that only jumps when touching ground
			if (Input.GetKeyDown (KeyCode.W) && !is_jumping) {
				rigidbody2D.AddForce(Vector2.up*5.0f, ForceMode2D.Impulse);
			}

			// reset x mid-air
			if (Input.GetKeyDown (KeyCode.X) && is_jumping) {
				Vector2 new_vel = rigidbody2D.velocity;
				new_vel.x = 0.0f;
				rigidbody2D.velocity = new_vel;
			}
		}
	}

	// to be more accurate this should only consider collision with bottom surface
	void OnCollisionEnter2D(Collision2D collider){
		animator.SetBool("tombJump", false);
		is_jumping = false;
	}

	void OnCollisionExit2D(Collision2D collider) {
		is_jumping = true;
	}
}
