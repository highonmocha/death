﻿using UnityEngine;
using System.Collections;

public class TombstoneRespawn : MonoBehaviour {

	protected Animator animator;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	}

	void respawn()
	{
		Vector3 tombV = GameObject.Find("Tomb").transform.position;
		Vector3 v = transform.position;
		v.x = tombV.x;
		v.y = tombV.y;
		v.z = 0;
		transform.position = v;
		
		Vector2 vv = rigidbody2D.velocity;
		vv.y *= -1;
		rigidbody2D.velocity = vv;
		
		// tombJump is true
		animator.SetBool("tombJump", true);
	}

	void OnTriggerEnter2D(Collider2D collider){
		if (collider.gameObject.name == "death_cause") {
			respawn();		
		}
	}
}
